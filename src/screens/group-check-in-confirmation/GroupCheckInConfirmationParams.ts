import { User } from "../../models//User"; // Adjust the path as necessary
import { EntrantRecordList } from "../../models/EntrantRecordList"; // Ensure the import path is correct

export class GroupCheckInConfirmationParams {
  private _user: User = new User();
  private _eventId: number = 0;
  private _venueId: number = 0;
  private _receptionTypeCode: string = "";
  private _entrantRecordList: EntrantRecordList = new EntrantRecordList();
  private _selectedEntrantIndex: number = 0;

  get user(): User {
    return this._user;
  }

  set user(value: User) {
    this._user = value;
  }

  get eventId(): number {
    return this._eventId;
  }

  set eventId(value: number) {
    this._eventId = value;
  }

  get venueId(): number {
    return this._venueId;
  }

  set venueId(value: number) {
    this._venueId = value;
  }

  get receptionTypeCode(): string {
    return this._receptionTypeCode;
  }

  set receptionTypeCode(value: string) {
    this._receptionTypeCode = value;
  }

  get entrantRecordList(): EntrantRecordList {
    return this._entrantRecordList;
  }

  set entrantRecordList(value: EntrantRecordList) {
    this._entrantRecordList = value;
  }
  get selectedEntrantIndex(): number {
    return this._selectedEntrantIndex;
  }

  set selectedEntrantIndex(value: number) {
    this._selectedEntrantIndex = value;
  }
}
